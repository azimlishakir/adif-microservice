package auditing.model;

import lombok.*;

@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UsersLogin {

    private String username;

    private String password;

}
