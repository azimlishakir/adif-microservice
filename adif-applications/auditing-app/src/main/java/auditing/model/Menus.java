package auditing.model;

import javax.persistence.*;
import java.util.Arrays;

@Entity
public class Menus {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String url;

    @Lob
    private byte[] image;

    public Menus() {
        super();
    }

    public Menus(Long id, String name, String url, byte[] image) {
        this.id = id;
        this.name = name;
        this.url = url;
        this.image = image;
    }

    public Menus(String fileName, String contentType, byte[] bytes) {
        this.name=fileName;
        this.url = contentType;
        this.image=bytes;
    }

    public Menus(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Menus{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", url='" + url + '\'' +
                ", image=" + Arrays.toString( image ) +
                '}';
    }
}
