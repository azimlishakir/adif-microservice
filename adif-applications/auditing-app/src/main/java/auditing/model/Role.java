package auditing.model;

import org.hibernate.validator.constraints.EAN;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "roles")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

/*    @OneToMany(fetch = FetchType.LAZY,mappedBy = "role")
    private List<Users> users;*/

    @OneToMany(mappedBy = "role")
    private Collection<Users> aUserCollection;

    public Role(){}

    public Role(String name){this.name = name;}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
