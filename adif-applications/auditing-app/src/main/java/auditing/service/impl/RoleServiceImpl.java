package auditing.service.impl;

import auditing.model.Role;
import auditing.repository.RoleRepository;
import auditing.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public List listRole() {
        Iterable<Role> allRole = roleRepository.findAll();
        return Arrays.asList(allRole);
    }

    @Override
    public Role getRole(Long id) {
        Optional<Role> byId = roleRepository.findById(id);
        if (byId.isPresent()){
            return byId.get();
        }
        return null;
    }

    @Override
    public Role createRole(Role role) {
        if (role.getId() != null){
            System.out.println ("Role id must be null");
            role.setId(null);
        }
        return roleRepository.save(role);
    }

    @Override
    public Role updateRole(Role role, Long id) {
        role.setId(id);

        return roleRepository.save(role);
    }

    @Override
    public void deleteRole(Long id) {
        Optional<Role> byId = roleRepository.findById(id);
        if (byId.isPresent()){
            roleRepository.delete(byId.get());
        }else {
            System.out.println (" No role with id: " + id);
        }

    }
}
