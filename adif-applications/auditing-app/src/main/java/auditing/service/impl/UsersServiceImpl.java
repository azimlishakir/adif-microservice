package auditing.service.impl;

import auditing.exception.RoleNotFoundException;
import auditing.model.Role;
import auditing.model.Users;
import auditing.repository.RoleRepository;
import auditing.repository.UsersRepository;
import auditing.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class UsersServiceImpl implements UsersService {

    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private RoleRepository roleRepository;


    @Autowired
    @Lazy
    private PasswordEncoder passwordEncoder;

    @Override
    public List  listUsers() {
        Iterable<Users> allUsers = usersRepository.findAll ();
        return Arrays.asList (allUsers);
    }



    @Override
    public Users getUsers(Long id) {
        Optional<Users>  byId = usersRepository.findById (id);
        if (byId.isPresent ()){
            return byId.get ();
        }

        return null;
    }

    @Override
    public Users createUsers(Users users) {
        if (users.getId () != null){
            System.out.println ("Users id must be null");
        }
        Role role = roleRepository.findById(users.getRole().getId()).orElseThrow(null);
        users.setRole(role);

        users.setPassword(passwordEncoder.encode(users.getPassword()));
       return usersRepository.save (users);
    }

    @Override
    public Users updateUsers(Users users, Long id) {
       users.setId (id);
       users.setPassword( passwordEncoder.encode( users.getPassword()));
       return usersRepository.save (users);
    }

    @Override
    public void deleteUsers(Long id) {
       Optional<Users> byId = usersRepository.findById (id);
       if (byId.isPresent ()){
           usersRepository.delete (byId.get ());
       }else {
           System.out.println (" No users with id: " + id);
       }
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        Users user = usersRepository.findByUsername(s)
                .orElseThrow(() -> new UsernameNotFoundException("Invalid username"));

        return  user;
    }

    private Role findRoleWithName(String name) {
        Role role = roleRepository.findByName(name).orElseThrow(() ->
                new RoleNotFoundException(name));
        return role;

    }
}
