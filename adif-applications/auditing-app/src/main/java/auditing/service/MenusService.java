package auditing.service;

import auditing.model.Menus;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface MenusService {

    String createFile(MultipartFile file) throws IOException;

}
