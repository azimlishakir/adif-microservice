package auditing.service;

import auditing.model.Role;

import java.util.List;

public interface RoleService {
    List listRole();

    Role getRole(Long id);

    Role createRole(Role role);

    Role updateRole(Role role,Long id);

    void deleteRole(Long id);
}
