package auditing.controller;

import auditing.model.Menus;
import auditing.service.MenusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.io.*;

@RestController
@RequestMapping("menus")
public class MenusController {


    @Autowired
    private MenusService menusService;

    @PostMapping("/upload")
    public String upload(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) throws IOException {

            menusService.createFile(file);

            redirectAttributes.addFlashAttribute( "message","You successfully uploaded " + file.getOriginalFilename() );

        return "static/images/menus/";
        }
    }


