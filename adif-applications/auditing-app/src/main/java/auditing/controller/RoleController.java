package auditing.controller;

import auditing.model.Role;
import auditing.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/role")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @GetMapping("all")
    public ResponseEntity allRole(){
        return ResponseEntity.ok(roleService.listRole());
    }

    @GetMapping("{id}")
    public ResponseEntity<Role> getRole(@PathVariable("id") Long id){
        return ResponseEntity.ok(roleService.getRole(id));
    }

    @PostMapping
    public ResponseEntity<Role> createRole(@RequestBody Role role){
        System.out.println ("Role elave edildi " + role);
        return ResponseEntity.ok(roleService.createRole(role));
    }

    @PutMapping("{id}")
    public ResponseEntity<Role> updateRole(@RequestBody Role role,@PathVariable("id") Long id){
        return ResponseEntity.ok(roleService.updateRole(role,id));
    }

    void deleteRole(@PathVariable("id") Long id){
        roleService.deleteRole(id);
    }



}
