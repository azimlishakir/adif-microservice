package auditing.security;

public final class LoginJwtTokenResponse {

    private String accessToken;

    private String name;

    private String lastname;

    public LoginJwtTokenResponse(String accessToken, String name, String lastname) {
        this.accessToken = accessToken;
        this.name = name;
        this.lastname = lastname;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
}