package auditing;

import auditing.model.Role;
import auditing.model.Users;
import auditing.service.UsersService;
import org.apache.catalina.User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

@SpringBootApplication
@EnableElasticsearchRepositories
public class AuditingApplications {
    public static void main(String[] args) {
       SpringApplication.run(AuditingApplications.class,args);
    }
}
