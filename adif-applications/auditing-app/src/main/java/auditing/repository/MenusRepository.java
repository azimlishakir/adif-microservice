package auditing.repository;

import auditing.model.Menus;
import org.springframework.data.repository.CrudRepository;

public interface MenusRepository  extends CrudRepository<Menus,Long> {
}
